/**
 * 视图控制器
 *
 * 目的：为了使组件保持干净并专注于内容本身，将组件显示隐藏等操作从组件文件中抽象出来。从而减少每个组件的冗余代码与操作。
 *
 * 1、组件载入时"created"，为组件添加视图控制器方法，此方法将自动为组件挂载视图显示、隐藏与监听状态变化的回调事件。流程请看以下示例：
 *
 *    A组件载入
 *    ↓
 *    为A组件添加视图控制器
 *    ↓
 *    A.show()  A.hide()  A.event() ...   视图控制器为组件挂载了显示隐藏与组件内的事件回调等方法
 *    ↓
 *    将A组件挂载到全局事件总线this.$events
 *    ↓
 *    在全局任意地方可以使用全局事件总线访问A组件及其属性与方法
 *    ↓
 *    例如：
 *    this.$events.A.show() 显示A组件
 *    ↓
 *    例如：
 *    this.$events.A.hide() 隐藏A组件
 *    ↓
 *    并且在组件的显示或隐藏事件中都可以访问到组件的事件回调
 *    ↓
 *    例如：
 *    this.$events.A.show({
 *      on:{
 *        show(_this){
 *          // 当A组件显示时做些事情...
 *          // 并且可以访问到A组件本身_this
 *        },
 *        hide(_this){
 *          // 当A组件隐藏时做些事情...
 *        },
 *        event(name){
 *          // A组件内暴露的一些事件...
 *
 *          // 比如执行A组件内名字为login的事件
 *          if(name==='login'){
 *            // 去登录...
 *          }
 *        }
 *      }
 *    })
 *
 * 1、词汇说明：
 *
 *    状态码：
 *
 *    状态码有哪些？
 *    状态码"status"目前有0=删除，1=隐藏，2=显示
 *
 *    状态码为什么可以控制组件显示？
 *    在组件的总容器标签上写上 v-if="status" 和view="status"，当"status"=0时相当于v-if=，0就是false，此时组件就删除了。而当status大于0时，v-if相法于为true,就是显示状态；
 *    而view=1时。组件有配套的CSS中写了当[view="1"]时开始播放一个隐藏的animation动画，同样的当[view="2"]时播放一个显示动画
 *
 *    显示
 *    显示包含了一个从隐藏到显示的过程动画，当view=2时，触发一个显示出来的动画直到动画结束保持最后的显示状态
 *
 *    隐藏
 *    隐藏包含了一个从显示到隐藏的过程动画和最后的隐藏状态，此时组件DOM还存在于文档中。
 *
 *    删除
 *    如果组件直接从DOM删除，则无法播放隐藏动画，所以，在隐藏动画结束时，再将组件的DOM节点从文档中删除
 * */

let view = (_this) => {
  var amEnd = (function () {
    var evs = {
      WebkitAnimation: 'webkitAnimationEnd',
      animation: 'animationend'
    };
    for (var k in evs) {
      if (typeof evs[k] === 'string') {
        return evs[k];
      }
    }
  })();

  let addAnim = (_this) => {
    _this.$el && _this.$el.children && _this.$el.children[0].addEventListener(amEnd, _this.anim, false);
  };

  // 重置回调
  _this.on = { prep: () => {} };

  let pre = (param) => {
    if (param) {
      // 埋点
      param.track && _this.track.ev(param.track);

      // 数据
      if (param.data) {
        for (let key in param.data) {
          if ({}.hasOwnProperty.call(param.data, key)) {
            _this[key] = param.data[key];
          }
        }
      }

      // 回调
      Object.assign(_this.on, param.on);
    }
  };

  /**
   * 显示组件
   * @param param {Object} 入参
   * */
  _this.show = (param) => {
    _this.show.param = param;
    pre(param);
    if (_this.status !== 2 && _this.status !== 3) {
      // 显示
      _this.status = 2;
      // 前置工作
      _this.on.prep(_this.status, _this);
      _this.$nextTick(() => {
        // 绑定动画监听事件
        addAnim(_this, param);
      });
    }
  };

  /**
   * 显示组件
   * @param param {Object} 入参
   * */
  _this.hide = (param) => {
    _this.hide.param = param;
    pre(param);
    if (_this.status !== 0 && _this.status !== 1) {

      // 隐藏组件，但是组件DOM还在
      _this.status = 1;
      // 前置工作
      _this.on.prep(_this.status, _this);
      _this.$nextTick(() => {
        // 绑定动画监听事件
        addAnim(_this, param);
      });
    }
  };

  // 打开
  _this.open = (param) => {
    _this.open.param = param;
    pre(param);
    if (_this.status !== 2 && _this.status !== 3) {

      // 隐藏组件，但是组件DOM还在
      _this.status = 3;
      _this.on.open && _this.on.open(param);
    }
  };

  // 关闭
  _this.none = (param) => {
    _this.none.param = param;
    pre(param);
    if (_this.status !== 0) {

      // 隐藏组件，但是组件DOM还在
      _this.status = 0;
      _this.on.none && _this.on.none(param);
    }
  };

  /**
   * 监听动画结束事件
   * */
  _this.anim = () => {
    // 当动画结束时，如果当前组件的状态码是1则将已隐藏的组件彻底清场。
    if (_this.status === 1) {

      // 设置组件状态码为0时，组件彻底清场，DOM将从文档中清除
      _this.status = 0;
      _this.on.hide && _this.on.hide(_this.hide.param);
    }
    if (_this.status === 2) {
      _this.on.show && _this.on.show(_this.hide.param);
    }

    _this.$el && _this.$el.children && _this.$el.children[0].removeEventListener(amEnd, _this.anim, false);
  };

  _this.event = (type) => {
    _this.on.event && _this.on.event(type, _this);
  };

  return _this;

};

export default view;
